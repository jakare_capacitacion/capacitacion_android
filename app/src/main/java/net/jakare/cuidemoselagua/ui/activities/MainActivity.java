package net.jakare.cuidemoselagua.ui.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.jakare.cuidemoselagua.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;

    private TextView mTextViewLogs;
    private Button mButtonUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        initViews();

        mButtonUpdate.setOnClickListener(this);
    }

    private void initViews() {
        mTextViewLogs = (TextView) findViewById(R.id.textview_log);
        mButtonUpdate = (Button) findViewById(R.id.button_update);
    }


    private void updateFromWs() {
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_update:
                updateFromWs();
                break;
            default:
                break;
        }
    }
}
